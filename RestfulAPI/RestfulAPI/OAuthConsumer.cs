﻿using RestfulAPI.Models;
using RestfulAPI.Util;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace RestfulAPI
{
    public class OAuthConsumer
    {
        private string _baseUrl;

        public OAuthConsumer()
        {
            _baseUrl = ConfigurationManager.AppSettings["ServerAddress"];
            if (_baseUrl[_baseUrl.Length - 1].Equals('/'))
                _baseUrl = _baseUrl.Substring(0, _baseUrl.Length - 1);
        }

        public AccessToken GetAccessToken(string clientId, string userName, string password)
        {
            if (String.IsNullOrEmpty(clientId) || String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(password))
                throw new ArgumentNullException("One of the supplied arguments was null or empty");

            RequestToken requestToken = GetRequestToken(clientId, userName, password);
            if (requestToken == null)
                return null;

            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(_baseUrl + requestToken.tokenUrl);
            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
            string text = "";
            if (response.GetResponseStream() != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
            }
            var accToken = JsonHelper.DeserializeJson<AccessToken>(text, true);
            accToken.ExpireTime = DateTime.Now.AddSeconds(accToken.expires_in);
            return accToken;
        }

        private RequestToken GetRequestToken(string clientId, string userName, string password)
        {
            var cryptograph = new Cryptograph();
            var user = new OAuthUser
                       {
                           UserName = userName,
                           Password = password
                       };
            string jsonString = JsonHelper.SerializeJson<OAuthUser>(user);

            HttpWebRequest httpWReq =
                (HttpWebRequest)WebRequest.Create(_baseUrl + "/authorize?response_type=code&client_id="
                                                   + clientId + "&redirect_url=");

            UTF8Encoding encoding = new UTF8Encoding();

            byte[] data = encoding.GetBytes(jsonString);

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json; charset=utf-8";
            httpWReq.ContentLength = data.Length;

            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

            string text = "";
            if (response.GetResponseStream() != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
            }

            return JsonHelper.DeserializeJson<RequestToken>(text, true);
        }
    }
}
