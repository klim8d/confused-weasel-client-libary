﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RestfulAPI.Util
{
    public class Cryptograph
    {
        public byte[] HashPassword(string password, byte[] salt)
        {
            var byteEncoder = new UTF8Encoding();
            byte[] key = byteEncoder.GetBytes(password);
            return CryptSharp.Utility.SCrypt.ComputeDerivedKey(key, salt, 262144, 8, 1, null, 128);
        }

        public byte[] GenerateSalt()
        {
            var rngCrypt = new RNGCryptoServiceProvider();
            byte[] salt = new byte[16];
            rngCrypt.GetBytes(salt);
            return salt;
        }

    }
}
