﻿using RestfulAPI.Models;
using RestfulAPI.Util;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace RestfulAPI
{
    public class UserData
    {
        private string _baseUrl;

        public UserData()
        {
            _baseUrl = ConfigurationManager.AppSettings["ServerAddress"];
            if (_baseUrl[_baseUrl.Length - 1].Equals('/'))
                _baseUrl = _baseUrl.Substring(0, _baseUrl.Length - 1);
        }

        public string CreateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("The supplied argument of the type User, was null");

            string jsonUser = JsonHelper.SerializeJson<User>(user);
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(_baseUrl + "/user/create");

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(jsonUser);

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json; charset=utf-8";
            httpWReq.ContentLength = data.Length;

            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }

        public User GetUserById(int id)
        {
            return JsonHelper.DeserializeJson<User>(String.Format(_baseUrl + "/user/get/{0}", id));
        }

        public User GetUserByAccessToken(AccessToken accessToken)
        {
            if (accessToken == null)
                throw new ArgumentNullException("The supplied argument of the type AccessToken, was null");

            return JsonHelper.DeserializeJson<User>(String.Format(_baseUrl + "/user/get?token={0}", accessToken.access_token));
        }

        public string DeleteUser(int id)
        {
            string jsonId = JsonHelper.SerializeJson<int>(id);
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(_baseUrl + "/user/delete");

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(jsonId);

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json; charset=utf-8";
            httpWReq.ContentLength = data.Length;

            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
}
