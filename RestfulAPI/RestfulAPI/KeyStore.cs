﻿using RestfulAPI.Models;
using RestfulAPI.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace RestfulAPI
{
    public class KeyStore
    {
        private string _baseUrl;

        public KeyStore()
        {
            _baseUrl = ConfigurationManager.AppSettings["ServerAddress"];
            if (_baseUrl[_baseUrl.Length - 1].Equals('/'))
                _baseUrl = _baseUrl.Substring(0, _baseUrl.Length - 1);
        }

        public bool CreateKey(UserKeyObejct ukO)
        {
            if (ukO == null)
                throw new ArgumentNullException("The supplied argument of the type UserKeyObejct, was null");

            string jsonString = JsonHelper.SerializeJson<UserKeyObejct>(ukO);
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(_baseUrl + "/keystore/create");

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(jsonString);

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json; charset=utf-8";
            httpWReq.ContentLength = data.Length;

            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

            string content = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return content.Contains("saved");
        }

        public PGPKey GetKeyByEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
                throw new ArgumentNullException("The supplied argument was null or empty");

            return JsonHelper.DeserializeJson<PGPKey>(String.Format(_baseUrl + "/keystore/get?email={0}", email));
        }

        public List<PGPKey> GetKeysByUser(User user)
        {
            //later
            return null;
        }

        public bool DeleteKey(PartialKey key)
        {
            if (key == null)
                throw new ArgumentNullException("The supplied argument of the type PartialKey, was null");

            string jsonId = JsonHelper.SerializeJson<PartialKey>(key);
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(_baseUrl + "/keystore/delete");

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(jsonId);

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json; charset=utf-8";
            httpWReq.ContentLength = data.Length;

            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

            string content = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return content.Contains("success");
        }

        public bool UpdateKey(UserKeyObejct ukO)
        {
            if (ukO == null)
                throw new ArgumentNullException("The supplied argument of the type UserKeyObejct, was null");

            string jsonString = JsonHelper.SerializeJson<UserKeyObejct>(ukO);
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(_baseUrl + "/keystore/update");

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(jsonString);

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json; charset=utf-8";
            httpWReq.ContentLength = data.Length;

            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

            string content = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return content.Contains("success");
        }
    }
}
