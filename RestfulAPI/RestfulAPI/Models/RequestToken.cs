﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RestfulAPI.Models
{
    [DataContract(IsReference = false)]
    public class RequestToken
    {
        [DataMember]
        public string tokenUrl { get; set; }
    }
}
