﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using RestfulAPI.Util;

namespace RestfulAPI.Models
{
    [DataContract(IsReference = false)]
    public class AccessToken
    {
        [DataMember]
        public string access_token { get; set; }
        
        [DataMember]
        public int expires_in { get; set; }

        [DataMember]
        public string refresh_token { get; set; }

        [DataMember]
        public string token_type { get; set; }

        public DateTime ExpireTime { get; set; }

        public void Refresh()
        {
            try
            {
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create("http://localhost:5000/token?grant_type=refresh_token&refresh_token=" + refresh_token);
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string text = "";
                if (response.GetResponseStream() != null)
                {
                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        text = sr.ReadToEnd();
                    }
                }
                var accToken = JsonHelper.DeserializeJson<AccessToken>(text, true);
                accToken.ExpireTime = DateTime.Now.AddSeconds(accToken.expires_in);
                access_token = accToken.access_token;
                expires_in = accToken.expires_in;
                refresh_token = accToken.refresh_token;
                token_type = accToken.token_type;
                ExpireTime = accToken.ExpireTime;
            }
            catch (Exception)
            {
                //throw / log
            }
        }
    }
}
