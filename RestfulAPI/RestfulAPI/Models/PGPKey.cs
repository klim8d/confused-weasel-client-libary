﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RestfulAPI.Models
{
    [DataContract(IsReference = false)]
    public partial class PGPKey
    {
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public DateTime? UpdatedAt { get; set; }
    }

    public class PartialKey
    {
        [DataMember]
        public string Email { get; set; }
    }
}
