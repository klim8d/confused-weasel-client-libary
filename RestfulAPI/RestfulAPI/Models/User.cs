﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace RestfulAPI.Models
{
    [DataContract(IsReference = false)]
    public partial class User
    {
        [DataMember]
        public string UserUuid { get; set; }

        [DataMember]
        public string UserName { get; set; }

        //[DataMember]
        //public byte[] Password { get; set; }

        //[DataMember]
        //public byte[] Salt { get; set; }

        public List<PGPKey> Keys { get; set; } 

        [DataMember]
        public DateTime? UpdatedAt { get; set; }
    }

    public class UserKeyObejct
    {
        public User User { get; set; }
        public PGPKey PGPKey { get; set; }
    }
}
