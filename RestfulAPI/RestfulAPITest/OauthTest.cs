﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestfulAPI;
using RestfulAPI.Models;

namespace RestfulAPITest
{
    [TestClass]
    public class OauthTest
    {
        private string _clientId = "d622b450-cfac-11e3-9c1a-0800200c9a66";
        private string _user = "JohnDoe";
        private string _password = "ThisIsASuperSecurePassword";

        [TestMethod]
        public void TestAutherize()
        {
            var auth = new RestfulAPI.OAuthConsumer();
            AccessToken acc = auth.GetAccessToken(_clientId, _user, _password);
            Assert.IsTrue(acc != null);
        }

        [TestMethod]
        public void TestAutherizeFailiure()
        {
            var auth = new RestfulAPI.OAuthConsumer();
            AccessToken acc = auth.GetAccessToken(_clientId, _user, _password);
            Assert.IsFalse(acc != null);
        }

        [TestMethod]
        public void TestAccessTokenRefresh()
        {
            var oauth = new OAuthConsumer();
            var accesstoken = oauth.GetAccessToken(_clientId, _user, _password);
            var var1 = accesstoken.access_token;
            accesstoken.Refresh();
            Assert.IsFalse(var1.Equals(accesstoken.access_token));
        }
    }
}
