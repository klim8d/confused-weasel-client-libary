﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestfulAPI;
using RestfulAPI.Models;


namespace RestfulAPITest
{
    [TestClass]
    public class UserDataTest
    {
        private string _clientId = "d622b450-cfac-11e3-9c1a-0800200c9a66";
        private string _user = "JohnDoe";
        private string _password = "ThisIsASuperSecurePassword";

        [TestMethod]
        public void TestCreateUser()
        {
            var ud = new UserData();
            var user = new User();
            user.UserName = "ChrisDoe";
            var ok = ud.CreateUser(user);
            Assert.IsTrue(ok.Equals("(status:succes)"));
        }

        [TestMethod]
        public void TestDeleteUser()
        {
            //Uuid is needed to delete user :(
        }

        [TestMethod]
        public void TestGetUserByAccessToken()
        {
            var ud = new UserData();
            var oAuthConsumer = new OAuthConsumer();
            var accesstoken = oAuthConsumer.GetAccessToken(_clientId, _user, _password);
            User user = ud.GetUserByAccessToken(accesstoken);
            Assert.IsNotNull(user);
        }
    }
}
