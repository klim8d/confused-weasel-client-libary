﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestfulAPI;
using RestfulAPI.Models;

namespace RestfulAPITest
{
    [TestClass]
    public class KeyStoreTest
    {
        [TestMethod]
        public void TestCreateKey()
        {
            var ukO = new UserKeyObejct
                      {
                          User = new User
                                 {
                                     UserUuid = "cd3fe37b-a7a9-47da-6344-5459d5b18922",
                                     UserName = "ChrisDoe"
                                 },
                          PGPKey = new PGPKey
                          {
                              Email = "Bogus@mail.com",
                              Key = "23a"
                          }
                      };
            var ks = new KeyStore();
            var status = ks.CreateKey(ukO);
            Assert.IsTrue(status);
        }

        [TestMethod]
        public void TestGetKey()
        {
            var ks = new KeyStore();
            string email = "Bogus@mail.com";
            var ok = ks.GetKeyByEmail(email);
            Assert.IsTrue(ok.Email.Equals(email));
        }

        [TestMethod]
        public void TestDeleteKey()
        {
            var ks = new KeyStore();
            var key = new PartialKey
                      {
                          Email = "Bogus@mail.com"
                      };
            var status = ks.DeleteKey(key);
            Assert.IsTrue(status);
        }
    }
}
