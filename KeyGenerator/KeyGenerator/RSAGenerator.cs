﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Utilities.IO.Pem;
using Org.BouncyCastle.Utilities.IO;

namespace KeyGenerator
{
    public struct PGPKeyPair
    {
        public PgpPublicKey PublicKey { get; set; }
        public PgpSecretKey PrivateKey { get; set; }
    }

    public class RSAGenerator
    {
        public PGPKeyPair GenerateKey(string username, string password, Stream privateKey, Stream publicKey)
        {
            IAsymmetricCipherKeyPairGenerator kpg = GeneratorUtilities.GetKeyPairGenerator("RSA");
            kpg.Init(new RsaKeyGenerationParameters(BigInteger.ValueOf(0x10001), SecureRandom.GetInstance("SHA256PRNG"), 2048, 25));
            AsymmetricCipherKeyPair kp = kpg.GenerateKeyPair();

            return ExportKeyPair(privateKey, publicKey, kp.Public, kp.Private, username, password.ToCharArray(), true);
        }

        public PGPKeyPair GenerateKey(string username, string password, string keyStoreUrl, string keyname)
        {
            IAsymmetricCipherKeyPairGenerator kpg = GeneratorUtilities.GetKeyPairGenerator("RSA");
            kpg.Init(new RsaKeyGenerationParameters(BigInteger.ValueOf(0x10001), SecureRandom.GetInstance("SHA256PRNG"), 2048, 25));
            AsymmetricCipherKeyPair kp = kpg.GenerateKeyPair();

            var pubLoc = string.Format(@"{0}\{1}.pub.asc", keyStoreUrl, keyname);
            var privLoc = string.Format(@"{0}\{1}.asc", keyStoreUrl, keyname);

            FileStream out1 = new FileInfo(privLoc).OpenWrite();
            FileStream out2 = new FileInfo(pubLoc).OpenWrite();

            return ExportKeyPair(out1, out2, kp.Public, kp.Private, username, password.ToCharArray(), true);
        }

        private PGPKeyPair ExportKeyPair(Stream secretOut, Stream publicOut, AsymmetricKeyParameter publicKey,
            AsymmetricKeyParameter privateKey, string identity, char[] passPhrase, bool armor)
        {
            if (armor)
            {
                secretOut = new ArmoredOutputStream(secretOut);
                publicOut = new ArmoredOutputStream(publicOut);
            }

            PgpSecretKey secretKey = new PgpSecretKey(
                PgpSignature.DefaultCertification,
                PublicKeyAlgorithmTag.RsaGeneral,
                publicKey,
                privateKey,
                DateTime.Now,
                identity,
                SymmetricKeyAlgorithmTag.Cast5,
                passPhrase,
                null,
                null,
                new SecureRandom()
                );

            secretKey.Encode(secretOut);
            secretOut.Close();

            PgpPublicKey key = secretKey.PublicKey;
            key.Encode(publicOut);
            publicOut.Close();

            return new PGPKeyPair { PublicKey = key, PrivateKey = secretKey };
        }
    }
}
