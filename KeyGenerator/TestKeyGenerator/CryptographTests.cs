﻿using System;
using KeyGenerator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.IO;

namespace TestKeyGenerator
{
    [TestClass]
    public class CryptographTests
    {
        [TestMethod]
        public void TestCreateKeys()
        {
            RSAGenerator rsa = new RSAGenerator();
            rsa.GenerateKey("testuser", "omg123", @"C:\rsakeys", "testkeys");
        }

        [TestMethod]
        public void TestCreateKeysToStream()
        {
            RSAGenerator rsa = new RSAGenerator();
            var privStream = new MemoryStream();
            var pubStream = new MemoryStream();

            var keys = rsa.GenerateKey("testuser", "omg123", privStream, pubStream);
            pubStream.Position = 0;
            privStream.Position = 0;
            string pubString = Encoding.UTF8.GetString(pubStream.ToArray());
            string privString = Encoding.UTF8.GetString(privStream.ToArray());
            var pubKey = pubStream.ImportPublicKey();
            var privKey = privStream.ImportSecretKey();
        }

        [TestMethod]
        public void TestEncrypt()
        {
            string data = "This is a simple test of encrypting a string";
            string password = "omg123";
            string pubKey;
            string privateKey;
            string encryptedData;

            using (var streamReader = new StreamReader(@"C:\rsakeys\testkeys.asc", Encoding.UTF8))
            {
                pubKey = streamReader.ReadToEnd();
            }

            using (var streamReader = new StreamReader(@"C:\rsakeys\testkeys.pub.asc", Encoding.UTF8))
            {
                privateKey = streamReader.ReadToEnd();
            }

            using (var stream = pubKey.Streamify())
            {
                var key = stream.ImportPublicKey();
                using (var clearStream = data.Streamify())
                using (var cryptoStream = new MemoryStream())
                {
                    clearStream.PgpEncrypt(cryptoStream, key);
                    cryptoStream.Position = 0;
                    encryptedData = cryptoStream.Stringify();
                }
            }

            Stream outputStream = null;
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(encryptedData)))
            {
                outputStream = stream.PgpDecrypt(privateKey, password);
            }

            string decryptedData = outputStream.Stringify();

            Assert.AreEqual(data, decryptedData);
        }

        [TestMethod]
        public void TestDecryptFail()
        {
            string data = "This is a simple test of encrypting a string";
            string password = "wrongpassword";
            string pubKey;
            string privateKey;
            string encryptedData;
            Stream outputStream = null;

            using (var streamReader = new StreamReader(@"C:\rsakeys\testkeys.asc", Encoding.UTF8))
            {
                pubKey = streamReader.ReadToEnd();
            }

            using (var streamReader = new StreamReader(@"C:\rsakeys\testkeys.pub.asc", Encoding.UTF8))
            {
                privateKey = streamReader.ReadToEnd();
            }

            try
            {
                using (var stream = pubKey.Streamify())
                {
                    var key = stream.ImportPublicKey();
                    using (var clearStream = data.Streamify())
                    using (var cryptoStream = new MemoryStream())
                    {
                        clearStream.PgpEncrypt(cryptoStream, key);
                        cryptoStream.Position = 0;
                        encryptedData = cryptoStream.Stringify();
                    }
                }

                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(encryptedData)))
                {
                    outputStream = stream.PgpDecrypt(privateKey, password);
                }
            }
            catch (Exception)
            {

            }

            string decryptedData = outputStream != null ? outputStream.Stringify() : "";

            Assert.AreNotEqual(data, decryptedData);
        }
    }
}
